# Keyword Extraction

This code provides a python class for taking raw text, applying heuristics to it, and then generating keywords from the text via Comprehend. Keywords can be used on ingest to tag documents or as part of search results to give users a quick sense of what is inside of a document. Just like Summary Extractor, this class is part of a larger summarization app.
Comprehend generates keywords as part of the API, however, in practice using rules to only inlcude relevant words, helps produce better keyword summaries.
This script uses the NLTK library to clean, remove stop words, and lemmatize text prior to sending it to Comprehend via the Boto3 API. Furthermore, a minimum confidence level and desired length of output is set for Comprehend to ensure that the most meaningufl results come back.

## Packages

Package versions are:

```
boto3==1.4.4
pandas==0.22.0
nltk==3.2.5
```

## Usage

```
comprehend = boto3.client(service_name='comprehend', region_name='us-east-1')

text = '''Coastal uplift can explain both paradoxes. Cenozoic uplift and exhumation of the north rim of the
Gulf of Mexico created the observed coastal offlap and truncation around the rim.
Uplift tilted the continental margin and overpowered the influence of the paleo shelf break,
causing shortening much farther up-dip than before uplift.'''

ke = KeywordsExtractor(comprehend, ['stop','word','list'])

print(ke)
{'keywords':  'Coastal uplift, both paradox Cenozoic uplift, the north rim, the Gulf,
the observed coastal offlap,the rim Uplift, the continental margin, the influence, the paleo shelf break',
'min_confidence_score': 0.8}
```


