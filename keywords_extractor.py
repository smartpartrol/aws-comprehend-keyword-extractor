import boto3
import nltk
import pandas as pd
from nltk import WordNetLemmatizer, word_tokenize, re

class KeywordsExtractor:

    """Extract keywords from text via Comprehend engine with heurstics

    Arguments
    ----------

    comprehend_client : str, client
        Preconfigured Boto3 Comprehend client

    min_terms: int
        Minimum word length to return, defailts to combinations of 2 or more words

    max_terms: int
        Maximum word length to return, defaults to up to 5 word combinations

    min_confidence_score: float
        Cutoff score for Comprehend confidence level in keyword relevance

    stop_words: list
        List of stop words to remove before passing to Comprehend

    Examples
    --------

    >>> comprehend = boto3.client(service_name='comprehend', region_name='us-east-1')

    >>> text = '''Coastal uplift can explain both paradoxes. Cenozoic uplift and exhumation of the north rim of the
    Gulf of Mexico created the observed coastal offlap and truncation around the rim.
    Uplift tilted the continental margin and overpowered the influence of the paleo shelf break,
    causing shortening much farther up-dip than before uplift.'''

    >>> ke = KeywordsExtractor(comprehend, ['stop','word','list'])

    >>> {'keywords':  'Coastal uplift, both paradox Cenozoic uplift, the north rim, the Gulf,
    the observed coastal offlap,the rim Uplift, the continental margin, the influence, the paleo shelf break',
        'min_confidence_score': 0.8}
    """

    def __init__(self, comprehend_client, stop_words):
        self.comprehend_client = comprehend_client
        self.min_terms = 2
        self.max_terms = 5
        self.min_confidence_score = 0.8
        self.stop_words = stop_words
        nltk.download('wordnet')

    def _preprocess_sentence(self, text_line):
        lemmatizer = WordNetLemmatizer()
        cleaned_txt_line = re.sub(r'[^a-zA-Z0-9\[\]]', ' ', text_line)
        word_tokens = word_tokenize(cleaned_txt_line)
        tokens_without_stopwords = (token for token in word_tokens if token.lower() not in self.stop_words)
        lemmatized_tokens = map(lemmatizer.lemmatize, tokens_without_stopwords)
        return ' '.join(lemmatized_tokens)

    def _extract_keywords(self, text):
        # TODO: send in batches
        response = self.comprehend_client.detect_key_phrases(Text=text, LanguageCode='en')
        return pd.io.json.json_normalize(response['KeyPhrases'])


    def _filter_keywords(self, keywords_df):
        keywords_df['word_count'] = keywords_df['Text'].str.split().apply(len).values
        filtered_df = keywords_df[
            (keywords_df['word_count'] >= self.min_terms) &
            (keywords_df['word_count'] <= self.max_terms) &
            (keywords_df['Score'] >= self.min_confidence_score)
        ]
        results = filtered_df[['Text']].values.T.tolist()[0]
        return results

    def extract(self, text):
        sentences = text.split('.')
        clean_sentences = map(self._preprocess_sentence, sentences)
        cleaned_text = ' '.join(clean_sentences)
        keywords_df = self._extract_keywords(cleaned_text)
        keywords = self._filter_keywords(keywords_df)
        keywords = ', '.join(map(str, keywords))

        return {
            'keywords': keywords,
            'min_confidence_score': self.min_confidence_score
        }